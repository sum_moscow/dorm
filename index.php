
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Общежития ГУУ</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Bootstrap theme -->

	<!-- Custom styles for this template -->
	<link href="css/theme.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">

	<link rel="shortcut icon" href="favicon.ico">
</head>

<body role="document">

<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Общежития ГУУ</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Общежития ГУУ</a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown select-dorm dorm-2">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Общежитие №2 <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
					</ul>
				</li>
				<li class="dropdown select-dorm dorm-6">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Общежитие №6 <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
					</ul>
				</li>
				<li class="show-queue"><a href="#">Очередь на заселение</a></li>
			</ul>
			<form class="navbar-form navbar-right">
				<input type="text" class="form-control" placeholder="Поиск...">
			</form>
		</div><!--/.nav-collapse -->
	</div>
</div>

<div class="mn-cnt" role="main">
	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<h1>Выберите общежитие и этаж!</h1>
		<p>Ну короче приветствие, потом заполню как надо пользоваться.</p>
	</div>
</div>



<?php
	require 'tmpl/footer.html';
?>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script src="javascript/bootstrap.min.js"></script>
<script src="javascript/app.js"></script>
<script src="javascript/pace.min.js"></script>

<script src="javascript/modernizr.custom.js"></script>
<script src="javascript/classie.js"></script>
<script src="javascript/notificationFx.js"></script>
<script src="javascript/typeahead.bundle.js"></script>
</body>
</html>
