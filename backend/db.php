<?php
	$dbuser = 'root'; // Введите имя вашего пользователя БД MySQL
	$dbpass = ''; // Ввелите пароль вашего пользователя БД MySQL
	$db = new PDO('mysql:host=localhost;dbname=dorm;charset=utf8', $dbuser, $dbpass);
	$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
	$db->exec("set names utf8");
	$db->exec("SET GLOBAL general_log = 1;");
	define('SITE_URL', "http://{$_SERVER['HTTP_HOST']}/");
	session_start();
