<?php

require_once 'Class.AbstractException.php';

class DBQueryException extends AbstractException{

	public function __construct($message, $level = 500, PDO $db, $user_message = '', $file = '', $file_line = 0){
		if (is_array($message)){
			$message = implode(';', $message);
		}elseif ($message == ''){
			$message = implode(';', $db->errorInfo());
		}
		if ($user_message == ''){
			$this->user_message = 'Извините, произошла ошибка. Мы уже исправляем ситуацию.';
		}else{
			$this->user_message = $user_message;
		}
		parent::__construct($message, $level, $db, $file, $file_line);
	}

}