<?php

	class Logger
	{
		public static function log($act, $request, PDO $db, Exception $e = null){
			$with_exception = ($e instanceof Exception) && ($e != null)?1:0;
			$exception_message = ($with_exception == 1)?"Message: {$e->getMessage()}\n Code: {$e->getCode()}\n File: {$e->getFile()}:{$e->getLine()}\n Trace: {$e->getTraceAsString()}":'';
			$request_line = array();

			foreach ($request as $key => $val){
				$request_line [] = "{$key}={$val}";
			}
			$request_line = implode('&', $request_line);

			$q_ins_action = 'INSERT INTO logs(act_name, with_exception, exception_message, request_arguments)
				VALUES(:act_name, :with_exception, :exception_message, :request_arguments)';
			$p_ins_action = $db->prepare($q_ins_action);
			$p_ins_action->execute(array(':act_name' => $act, ':with_exception' => $with_exception, ':exception_message' => $exception_message,':request_arguments' => $request_line));
		}

	}