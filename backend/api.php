<?php
	require_once 'db.php';
	require_once 'Class.Result.php';
	require_once 'Class.Dorm.php';
	require_once 'Class.Room.php';
	require_once 'Class.Block.php';
	require_once 'Class.Floor.php';
	require_once 'Class.Student.php';
	require_once 'Class.Logger.php';
	require_once 'Class.Queue.php';
	require_once 'Exceptions/Class.AbstractException.php';
	require_once 'Exceptions/Class.DBQueryException.php';
	$result = array();
	$request_data = $_REQUEST;

	function checkExistenceInRequest(array $params){
		foreach($params as $key => $value){
			if (!isset($_REQUEST[$key]) || $_REQUEST[$key] == ''){
				throw new InvalidArgumentException($value);
			}
		}
	}

	try{
		switch ($_REQUEST['act']){
			case 'dorms.getFloorsList': {
				if (isset($_REQUEST['dorm_number'])){
					if (is_array($_REQUEST['dorm_number'])){
						$dorm_numbers = $_REQUEST['dorm_number'];
					}else{
						$dorm_numbers = array();
						$dorm_numbers[] = $_REQUEST['dorm_number'];
					}
				}else{
					$dorms = array(2, 6);
				}
				$result_array = array();
				foreach($dorms as $dorm_number){
					$dorm = new Dorm($dorm_number, $db);
					$result_array["{$dorm_number}"] = $dorm->getFloorsList();
				}
				$result = new Result(true, 'Данные успешно получены', array('items' => $result_array));
				break;
			}
			case 'dorm.addFloor': {
				checkExistenceInRequest(array(
					'dorm_number' => 'Не указан номер общежития',
					'floor_number' => 'Не указан номер этажа'
				));
				if (!isset($_REQUEST['floor_description'])) $_REQUEST['floor_description'] = '';
				$dorm = new Dorm($_REQUEST['dorm_number'], $db);
				$result = $dorm->addFloor($_REQUEST['floor_number'], $_REQUEST['floor_description']);
				break;
			}
			case 'floor.addBlock': {
				checkExistenceInRequest(array(
					'dorm_number' => 'Не указан номер общежития',
					'floor_number' => 'Не указан номер этажа',
					'number' => 'Не указан номер блока'
				));
				$floor = new Floor($_REQUEST['dorm_number'], $_REQUEST['floor_number'], $db);
				$result = $floor->addBlock($_REQUEST['number'], $_REQUEST['comment']);
				break;
			}
			case 'floor.getFullData': {
				checkExistenceInRequest(array(
					'dorm_number' => 'Не указан номер общежития',
					'floor_number' => 'Не указан номер этажа'
				));
				$floor = new Floor($_REQUEST['dorm_number'], $_REQUEST['floor_number'], $db);
				$result = $floor->getFullData();
				break;
			}
			case 'rooms.getFullData': {
				checkExistenceInRequest(array(
					'room_full_id' => 'Не указан идентификатор комнаты'
				));
				$room = new Room($_REQUEST['room_full_id'], $db);
				$result = $room->getFullData();
				break;
			}
			case 'block.addRoom': {
				checkExistenceInRequest(array(
					'number' => 'Не указан идентификатор комнаты',
					'block_id' => 'Не указан идентификатор блока',
				));
				$block = new Block($_REQUEST['block_id'], $db);
				$result = $block->addRoom($_REQUEST);
				break;
			}
			case 'rooms.setArea': {
				checkExistenceInRequest(array(
					'value' => 'Не указано значение',
					'room_id' => 'Не указан идентификатор комнаты'
				));
				$room = new Room($_REQUEST['room_id'], $db);
				$room->setArea($_REQUEST['value']);
				$result = $room->update();
				break;
			}
			case 'rooms.setPlacesCount': {
				checkExistenceInRequest(array(
					'value' => 'Не указано значение',
					'room_id' => 'Не указан идентификатор комнаты'
				));
				$room = new Room($_REQUEST['room_id'], $db);
				$room->setPlacesCount($_REQUEST['value']);
				$result = $room->update();
				break;
			}
			case 'rooms.setRepairing': {
				checkExistenceInRequest(array(
					'value' => 'Не указано значение',
					'room_id' => 'Не указан идентификатор комнаты'
				));
				$room = new Room($_REQUEST['room_id'], $db);
				$room->setRepairing($_REQUEST['value']);
				$result = $room->update();
				break;
			}
			case 'rooms.addStudent': {
				checkExistenceInRequest(array(
					'room_id' => 'Не указана комната'
				));
				$room = new Room($_REQUEST['room_id'], $db);
				$result = $room->addStudent();
				break;
			}
			case 'rooms.removeStudent': {
				checkExistenceInRequest(array(
					'student_id' => 'Не указан студент',
					'reason' => 'Не указана причина'
				));
				$room = new Room($_REQUEST['room_id'], $db);
				$result = $room->removeStudent($_REQUEST['student_id'], $_REQUEST['reason']);
				break;
			}
			case 'queue.getFullData': {
				$queue = new Queue($db);
				$result = $queue->getFullData();
				break;
			}
			case 'queue.addStudent': {
				$queue = new Queue($db);
				checkExistenceInRequest(array(
					'name' => 'Не указано имя',
					'category' => 'Не указан статус проживающего',
					'inst_id' => 'Не указан институт'
				));
				$result = $queue->addStudent($_REQUEST);
				break;
			}
			case 'student.setComment': {
				checkExistenceInRequest(array(
					'value' => 'Не указано значение',
					'student_id' => 'Не указан идентификатор студента'
				));
				$student = new Student($_REQUEST['student_id'], $db);
				$student->setComment($_REQUEST['value']);
				$result = $student->update();
				break;
			}
			case 'student.getFullData': {
				checkExistenceInRequest(array(
					'student_id' => 'Не указан идентификатор студента'
				));
				$student = new Student($_REQUEST['student_id'], $db);
				$result = $student->getFullData();
				break;
			}
			case 'student.edit': {
				checkExistenceInRequest(array(
					'name' => 'Не указано имя студента',
					'student_id' => 'Не указан идентификатор студента',
					'inst_id' => 'Не указан институт студента',
					'sex' => 'Не указан пол'
				));
				$student = new Student($_REQUEST['student_id'], $db);
				$student->setComment($_REQUEST['comment']);
				$student->setCategory($_REQUEST['category']);
				$student->setCity($_REQUEST['city']);
				$student->setName($_REQUEST['name']);
				$student->setSex($_REQUEST['sex']);
				$student->setCourse($_REQUEST['course']);
				$student->setInstId($_REQUEST['inst_id']);
				$student->setOfficialAddress($_REQUEST['official_address']);
				$student->setPassportSer($_REQUEST['passport_ser']);
				$student->setPassportNumber($_REQUEST['passport_number']);
				$student->setPassportWho($_REQUEST['passport_who']);
				$student->setPassportWhen($_REQUEST['passport_when']);
				$student->setPhoneNumber($_REQUEST['phone_number']);
				$student->setDocument($_REQUEST['document']);
				$student->setPayDate($_REQUEST['pay_date']);
				$student->update();
				$room = new Room($student->getRoomId(), $db);
				$result = new Result(true, 'Данные успешно обноавлены', array(
					'dorm_number' => $room->getBlock()->getFloor()->getDormNumber(),
					'floor_number' => $room->getBlock()->getFloor()->getNumber()
				));
				break;
			}
		}
		echo $result;
		Logger::log($request_data['act'], $request_data, $db);
	}catch(Exception $e){
		echo new Result(false, 'Ошибка получения данных. ' . $e->getMessage());
		Logger::log($request_data['act'], $request_data, $db, $e);
	}