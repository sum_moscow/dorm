<?php

	class Student{

		private $id;
		private $name;
		private $sex;
		private $inst_id;
		private $inst_name;
		private $course;
		private $phone_number;
		private $document;
		private $city;
		private $comment;
		private $status;
		private $add_time;
		private $official_address;
		private $category;
		private $payed;
		private $pay_date;
		private $passport_ser;
		private $passport_number;
		private $passport_who;
		private $passport_when;
		private $room_id;
		private $db;

		public function __construct($id, PDO $db){
			$q_get_student = 'SELECT students.*, insts.abbr AS inst_name
				FROM students
				INNER JOIN insts ON insts.inst_id = students.inst_id
				WHERE student_id = :id AND students.status = 1';
			$p_get_student = $db->prepare($q_get_student);
			$p_get_student->execute(array(':id' => $id));
			if ($p_get_student === FALSE) throw new InvalidArgumentException('Неверный идентификатор студента');
			if ($p_get_student->rowCount() == 0) throw new InvalidArgumentException('Неверный идентификатор студента');
			$res = $p_get_student->fetch();

			$this->db = $db;
			$this->id = $res['student_id'];
			$this->name = $res['name'];
			$this->sex = $res['sex'];
			$this->inst_id = $res['inst_id'];
			$this->comment = $res['comment'];
			$this->course = $res['course'];
			$this->phone_number = $res['phone_number'];
			$this->document = $res['document'];
			$this->city = $res['city'];
			$this->room_id = $res['room_id'];
			$this->add_time = $res['add_time'];
			$this->official_address = $res['official_address'];
			$this->category = $res['category'];
			$this->payed = $res['payed'];
			$this->pay_date = $res['pay_date'];
			$this->passport_ser = $res['passport_ser'];
			$this->passport_number = $res['passport_number'];
			$this->passport_who = $res['passport_who'];
			$this->passport_when = $res['passport_when'];
			$this->status = $res['status'];
		}

		/**
		 * @param mixed $name
		 */
		public function setName($name) {
			$this->name = $name;
		}

		/**
		 * @param mixed $sex
		 */
		public function setSex($sex) {
			$this->sex = $sex;
		}

		/**
		 * @param mixed $add_time
		 */
		public function setAddTime($add_time) {
			$this->add_time = $add_time;
		}

		/**
		 * @return mixed
		 */
		public function getAddTime() {
			return $this->add_time;
		}

		/**
		 * @param mixed $category
		 */
		public function setCategory($category) {
			$this->category = $category;
		}

		/**
		 * @return mixed
		 */
		public function getCategory() {
			return $this->category;
		}

		/**
		 * @param mixed $city
		 */
		public function setCity($city) {
			$this->city = $city;
		}

		/**
		 * @return mixed
		 */
		public function getCity() {
			return $this->city;
		}

		/**
		 * @param \PDO $db
		 */
		public function setDb($db) {
			$this->db = $db;
		}

		/**
		 * @return \PDO
		 */
		public function getDb() {
			return $this->db;
		}

		/**
		 * @param mixed $official_address
		 */
		public function setOfficialAddress($official_address) {
			$this->official_address = $official_address;
		}

		/**
		 * @return mixed
		 */
		public function getOfficialAddress() {
			return $this->official_address;
		}

		/**
		 * @param mixed $passport_number
		 */
		public function setPassportNumber($passport_number) {
			$this->passport_number = $passport_number;
		}

		/**
		 * @return mixed
		 */
		public function getPassportNumber() {
			return $this->passport_number;
		}

		/**
		 * @param mixed $passport_ser
		 */
		public function setPassportSer($passport_ser) {
			$this->passport_ser = $passport_ser;
		}

		/**
		 * @return mixed
		 */
		public function getPassportSer() {
			return $this->passport_ser;
		}

		/**
		 * @param mixed $passport_when
		 */
		public function setPassportWhen($passport_when) {
			$this->passport_when = $passport_when;
		}

		/**
		 * @return mixed
		 */
		public function getPassportWhen() {
			return $this->passport_when;
		}

		/**
		 * @param mixed $passport_who
		 */
		public function setPassportWho($passport_who) {
			$this->passport_who = $passport_who;
		}

		/**
		 * @return mixed
		 */
		public function getPassportWho() {
			return $this->passport_who;
		}

		/**
		 * @param mixed $pay_date
		 */
		public function setPayDate($pay_date) {
			$this->payed = 1;
			$this->pay_date = $pay_date;
		}

		/**
		 * @return mixed
		 */
		public function getPayDate() {
			return $this->pay_date;
		}

		/**
		 * @param mixed $payed
		 */
		public function setPayed($payed) {
			$this->payed = $payed;
		}

		/**
		 * @return mixed
		 */
		public function getPayed() {
			return $this->payed;
		}

		/**
		 * @param mixed $status
		 */
		public function setStatus($status) {
			$this->status = $status;
		}

		/**
		 * @return mixed
		 */
		public function getStatus() {
			return $this->status;
		}

		/**
		 * @param mixed $comment
		 */
		public function setComment($comment) {
			$this->comment = $comment;
		}

		/**
		 * @param mixed $course
		 */
		public function setCourse($course) {
			$this->course = $course;
		}

		/**
		 * @param mixed $document
		 */
		public function setDocument($document) {
			$this->document = $document;
		}

		/**
		 * @param mixed $inst_id
		 */
		public function setInstId($inst_id) {
			$this->inst_id = $inst_id;
		}

		/**
		 * @param mixed $phone_number
		 */
		public function setPhoneNumber($phone_number) {
			$this->phone_number = $phone_number;
		}

		/**
		 * @param mixed $room
		 */
		public function setRoom(Room $room) {
			$this->room_id = $room->getRoomId();
		}

		/**
		 * @return mixed
		 */
		public function getComment() {
			return $this->comment;
		}
		/**
		 * @return mixed
		 */
		public function getCourse() {
			return $this->course;
		}

		/**
		 * @return mixed
		 */
		public function getDocument() {
			return $this->document;
		}

		/**
		 * @return mixed
		 */
		public function getId() {
			return $this->id;
		}

		/**
		 * @return mixed
		 */
		public function getInstId() {
			return $this->inst_id;
		}

		/**
		 * @return mixed
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @return mixed
		 */
		public function getPhoneNumber() {
			return $this->phone_number;
		}

		/**
		 * @return mixed
		 */
		public function getRoomId() {
			return $this->room_id;
		}

		/**
		 * @return mixed
		 */
		public function getSex() {
			return $this->sex;
		}

		/**
		 * @return mixed
		 */
		public function getInstName() {
			return $this->inst_name;
		}
		/**
		 * @return mixed
		 */
		public function getRoom() {
			return new Room($this->getRoomId(), $this->db);
		}


		public function getFullData(){
			return
				new Result (true, 'Данные успешно получены', array(
					'id' => $this->getId(),
					'name' => $this->getName(),
					'sex' => $this->getSex(),
					'inst_id' => $this->getInstId(),
					'course' => $this->getCourse(),
					'phone_number' => $this->getPhoneNumber(),
					'document' => $this->getDocument(),
					'city' => $this->getCity(),
					'comment' => $this->getComment(),
					'room_id' => $this->getRoomId(),
					'status' => $this->getStatus(),
					'payed' => $this->getPayed(),
					'inst_name' => $this->getInstName(),
					'official_address' => $this->getOfficialAddress(),
					'category' => $this->getCategory(),
					'pay_date' => $this->getPayDate(),
					'passport_ser' => $this->getPassportSer(),
					'passport_number' => $this->getPassportNumber(),
					'passport_who' => $this->getPassportWho(),
					'passport_when' => $this->getPassportWhen()
				)
			);
		}

		public function update(){
			$q_upd_student = 'UPDATE students SET
					name = :name,
					sex = :sex,
					inst_id = :inst_id,
					course = :course,
					phone_number = :phone_number,
					document = :document,
					city = :city,
					comment = :comment,
					room_id = :room_id,
					status = :status,
					official_address = :official_address,
					category = :category,
					payed = :payed,
					pay_date = :pay_date,
					passport_ser = :passport_ser,
					passport_number = :passport_number,
					passport_who = :passport_who,
					passport_when = :passport_when
				WHERE student_id = :student_id';
			$p_upd_student = $this->db->prepare($q_upd_student);
			$p_upd_student->execute(array(
				':name' => $this->getName(),
				':sex' => $this->getSex(),
				':inst_id' => $this->getInstId(),
				':course' => $this->getCourse(),
				':phone_number' => $this->getPhoneNumber(),
				':room_id' => $this->getRoomId(),
				':document' => $this->getDocument(),
				':city' => $this->getCity(),
				':comment' => $this->getComment(),
				':status' => $this->getStatus(),
				':official_address' => $this->getOfficialAddress(),
				':category' => $this->getCategory(),
				':payed' => $this->getPayed(),
				':pay_date' => $this->getPayDate(),
				':passport_ser' => $this->getPassportSer(),
				':passport_number' => $this->getPassportNumber(),
				':passport_who' => $this->getPassportWho(),
				':passport_when' => $this->getPassportWhen(),
				':student_id' => $this->getId()
			));
			if ($p_upd_student === FALSE) throw new DBQueryException('Ошибка обновления данных.', 100, $this->db);
			return new Result(true, 'Данные успешно обновлены', array(
				'dorm_number' => $this->getRoom()->getBlock()->getFloor()->getDormNumber(),
				'floor_number' => $this->getRoom()->getBlock()->getFloor()->getNumber()
			));
		}

	}