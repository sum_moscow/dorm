<?php

	class Queue{

		private $db;


		public function __construct(PDO $db){
			$this->db = $db;
		}


		public function getFullData(){
			$q_get_queue_list = 'SELECT queue.*, students.*
			FROM queue
			INNER JOIN students ON students.student_id = queue.student_id
			WHERE queue.status = 1 AND students.status=1';
			$p_get_queue_list = $this->db->prepare($q_get_queue_list);
			$p_get_queue_list->execute();
			if ($p_get_queue_list === FALSE) throw new DBQueryException('', 100, $this->db);

			$students = $p_get_queue_list->fetchAll();
			$female_count = $male_count = 0;
			foreach($students as $student){
				if ($student['sex'] == 0){
					$female_count++;;
				}else{
					$male_count++;
				}
			}

			return new Result(true, 'Данные успешно получены', array(
				'students' => $students,
				'male_count' => $male_count,
				'female_count' => $female_count,
				'all_count' => ($female_count + $male_count)
			));
		}


		public function addStudent($data){
			$q_ins_student = 'INSERT INTO
				students(name, sex, inst_id, course, phone_number, document, city, comment, room_id,
					official_address, category, payed, pay_date, passport_ser, passport_number, passport_who, passport_when)
				VALUES(:name, :sex, :inst_id, :course, :phone_number, :document, :city, :comment, :room_id,
					:official_address, :category, :payed, :pay_date, :passport_ser, :passport_number, :passport_who, :passport_when)';

			$q_ins_queue = 'INSERT INTO
				queue(student_id, status)
				VALUES(:student_id, 1)';

			$p_ins_student = $this->db->prepare($q_ins_student);
			$p_ins_queue = $this->db->prepare($q_ins_queue);
			$this->db->beginTransaction();

			$p_ins_student->execute(array(
				':name' => $data['name'],
				':sex' => $data['sex'],
				':inst_id' => $data['inst_id'],
				':course' => $data['course'],
				':phone_number' => $data['phone_number'],
				':document' => $data['document'],
				':city' => $data['city'],
				':comment' => $data['comment'],
				':room_id' => null,
				':official_address' => $data['official_address'],
				':category' => $data['category'],
				':payed' => ($data['pay_date'] == null || $data['pay_date'] == '') ? 0 : 1,
				':pay_date' => ($data['pay_date'] == null || $data['pay_date'] == '') ? null : $data['pay_date'],
				':passport_ser' => $data['passport_ser'],
				':passport_number' => $data['passport_number'],
				':passport_who' => $data['passport_who'],
				':passport_when' => ($data['passport_when'] == null || $data['passport_when'] == '') ? null : $data['passport_when'],
			));
			$p_ins_queue->execute(array(':student_id' => $this->db->lastInsertId()));

			$this->db->commit();

			if ($p_ins_student === FALSE) throw new DBQueryException('Ошибка добавления студента. Ошибка запроса к БД.', 100, $this->db);
			return new Result(true, 'Студент успешно добавлен в очередь');
		}


		public function fetchTop($sex = null){
			$full_res = $this->getFullData()->getData();
			$full_res = $full_res['students'];
			$queue_length = count($full_res);
			$next_student = null;
			$counter = 0;
			if ($sex != null){
				while($next_student == null && $counter < $queue_length){
					if ($full_res[$counter]['sex'] == $sex){
						$next_student = $full_res[$counter];
					}
					$counter++;
				}
			}else{
				$next_student = $full_res[0];
			}
			if ($next_student == null){
				throw new LogicException('Извините, но мы не смогли никого найти в очереди на заселение');
			}
			$q_upd_status = 'UPDATE queue SET status = 0 WHERE queue_id = :queue_id';
			$p_upd_status = $this->db->prepare($q_upd_status);
			$p_upd_status->execute(array(':queue_id' => $next_student['queue_id']));
			if ($p_upd_status === FALSE)throw new DBQueryException('Не удалось обновить данные', $this->db);
			return new Student($next_student['student_id'], $this->db);
		}


	}