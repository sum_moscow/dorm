<?php

	class Block{

		private $block_id;
		private $floor;
		private $number;
		private $comment;
		private $db;

		public function __construct($identifier, PDO $db){
			$q_get_block = 'SELECT blocks.block_id, blocks.number, blocks.comment, blocks.floor_id
				FROM blocks
				WHERE blocks.block_id = :block_identifier OR blocks.number = :block_identifier';
			$p_get_block = $db->prepare($q_get_block);
			$p_get_block->execute(array(':block_identifier' => $identifier));
			if ($p_get_block === FALSE){
				throw new DBQueryException('', 100, $db);
			}elseif($p_get_block->rowCount() != 1){
				throw new InvalidArgumentException('Неверный идентификатор блока');
			}
			$result = $p_get_block->fetch();

			$this->db = $db;
			$this->block_id = $result['block_id'];
			$this->floor = new Floor($result['floor_id'], null, $this->db);
			$this->number = $result['number'];
			$this->comment = $result['comment'];
		}

		public function addRoom($room_info){
			$q_ins_room = 'INSERT INTO rooms(block_id, number, area, comment, places_count, repairing, for_family)
					 VALUES (:block_id, :number, :area, :comment, :places_count, :repairing, :for_family)';
			$p_ins_room = $this->db->prepare($q_ins_room);

			$room_info['area'] = (is_numeric($room_info['area'])) ? (int) $room_info['area']: null;
			$room_info['places_count'] = (is_numeric($room_info['places_count'])) ? (int) $room_info['places_count']: null;
			$room_info['for_family'] = ($room_info['for_family'] == true || $room_info['for_family'] == 1) ? 1 : 0;
			$room_info['repairing'] = ($room_info['repairing'] == true || $room_info['repairing'] == 1) ? 1 : 0;

			$p_ins_room->execute(array(
				':block_id' => $this->getBlockId(),
				':number' => $room_info['number'],
				':area' => $room_info['area'],
				':comment' => $room_info['comment'],
				':places_count' => $room_info['places_count'],
				':repairing' => $room_info['repairing'],
				':for_family' => $room_info['for_family'],
			));
			if ($p_ins_room === FALSE) throw new DBQueryException('Не удалось создать комнату', 100, $this->db);
			return new Result(true, 'Комната успешно добавлена', array(
				'dorm_number' => $this->getFloor()->getDormNumber(),
				'floor_number' => $this->getFloor()->getNumber()
			));
		}

		/**
		 * @return mixed
		 */
		public function getBlockId() {
			return $this->block_id;
		}

		/**
		 * @return mixed
		 */
		public function getComment() {
			return $this->comment;
		}

		/**
		 * @return \Floor
		 */
		public function getFloor() {
			return $this->floor;
		}

		/**
		 * @return mixed
		 */
		public function getNumber() {
			return $this->number;
		}

		public function getStudentsSex(){
			$q_get_sex = 'SELECT sex FROM students
			INNER JOIN rooms on students.room_id = rooms.room_id
			INNER JOIN blocks on rooms.block_id = blocks.block_id
			WHERE students.status = 1 AND blocks.block_id= :block_id';
			$p_get_sex = $this->db->prepare($q_get_sex);
			$p_get_sex->execute(array(':block_id' => $this->getBlockId()));
			if ($p_get_sex === FALSE) throw new DBQueryException('Ощибка получения информации о жильцах', 100, $this->db);
			if ($p_get_sex->rowCount() == 0){
				return null;
			}else{
				return $p_get_sex->fetchColumn(0);
			}
		}
	}