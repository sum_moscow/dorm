<?php
class Result{
	private $status;
	private $text;
	private $data;
	private $error_code;

	public function __construct($status, $text = '', $data = '', $error_code = 0){
		$this->setStatus($status);
		$this->setText($text);
		$this->setErrorCode($error_code);
		$this->setData($data);
	}

	public function setStatus($status){
		$this->status = $status;
	}

	public function setText($text){
		$this->text = $text;
	}

	public function setErrorCode($error_code){
		$this->error_code = $error_code;
	}

	public function setData($data){
		$this->data = $data;
	}

	public function getData(){
		return $this->data;
	}

	public function addProp($name, $value){
		$this->data[$name] = $value;
	}

	public function __toString(){
		header("Content-Type: application/json");
		$arr = array('status' => $this->status, 'text' => $this->text, 'data' => $this->data);
		return json_encode($arr);
	}
}