<?php


class Floor{


	private $id;
	private $dorm_number;
	private $number;
	private $comment;
	private $db;

	public function __construct($dorm_number, $number, PDO $db){
		if (is_numeric($dorm_number) == false){
			throw new InvalidArgumentException('Неверный идентификатор этажа');
		}
		$where_param = ($number != null) ? 'floors.number = :number AND floors.dorm_number = :dorm_number' : 'floors.floor_id = :dorm_number';
		$params_array = array(':dorm_number' => $dorm_number);
		if($number != null){
			$params_array[':number'] = $number;
		}


		$q_get_floor = 'SELECT floor_id, number, dorm_number, comment
			FROM floors
			WHERE ' . $where_param;
		$p_get_floor = $db->prepare($q_get_floor);
		$p_get_floor->execute($params_array);

		if ($p_get_floor === FALSE) throw new DBQueryException('Не удалось выполнить запрос', 100, $db);
		if ($p_get_floor->rowCount() !== 1) throw new DBQueryException('Запрос вернул больше одного результата' , 100, $db);
		$r_get_floor = $p_get_floor->fetch();
		$this->db = $db;
		$this->number = $r_get_floor['number'];
		$this->dorm_number = $r_get_floor['dorm_number'];
		$this->comment = $r_get_floor['comment'];
		$this->id = $r_get_floor['floor_id'];
	}

	public function getFullData(){
		return new Result(true, 'Данные успешно получены',
			array(
				'floor_number' => $this->number,
				'dorm_number' => $this->dorm_number,
				'male_free_spaces' => null,
				'female_free_spaces' => null,
				'all_free_spaces' => $this->getFreeSpaces(),
				'students' => $this->getStudentsList()));
	}

	/**
	 * @return mixed
	 */
	public function getComment() {
		return $this->comment;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getDormNumber() {
		return $this->dorm_number;
	}

	/**
	 * @return mixed
	 */
	public function getNumber() {
		return $this->number;
	}

	//TODO: Сделать отображение по полам
	private function getFreeSpaces($sex = null){
		$q_get_all_spaces = 'SELECT SUM(places_count) FROM rooms
			INNER JOIN blocks on blocks.block_id = rooms.block_id
			INNER JOIN floors ON floors.floor_id = blocks.floor_id
			WHERE floors.floor_id = :floor_id';
		$p_get_spaces = $this->db->prepare($q_get_all_spaces);
		$p_get_spaces->execute(array(':floor_id' => $this->getId()));
		if ($p_get_spaces === FALSE) throw new DBQueryException('Ошибка запроса к БД. Попробуйте снова', 100, $this->db);

		$q_get_busy = 'SELECT COUNT(*) FROM students
			INNER JOIN rooms ON rooms.room_id = students.room_id
			INNER JOIN blocks on blocks.block_id = rooms.block_id
			INNER JOIN floors ON floors.floor_id = blocks.floor_id
			WHERE floors.floor_id = :floor_id AND students.status = 1';
		$p_get_busy = $this->db->prepare($q_get_busy);
		$p_get_busy->execute(array(':floor_id' => $this->getId()));
		if ($p_get_busy === FALSE) throw new DBQueryException('Ошибка запроса к БД. Попробуйте снова', 100, $this->db);
		return ($p_get_spaces->fetchColumn(0) - $p_get_busy->fetchColumn(0));
	}

	private function getStudentsList(){
		$q_get_students = 'SELECT students.*, CONCAT(blocks.number, "-", rooms.number) AS full_room_id,
			blocks.number AS block_number, rooms.*, rooms.number AS room_number, blocks.*,
			students.comment AS student_comment,
			insts.abbr AS inst_name
			FROM students
			  INNER JOIN insts on students.inst_id = insts.inst_id
			  RIGHT OUTER JOIN rooms ON students.room_id = rooms.room_id
			RIGHT OUTER JOIN blocks ON rooms.block_id = blocks.block_id
			WHERE blocks.floor_id = :floor_id';
		$p_get_students = $this->db->prepare($q_get_students);
		$p_get_students->execute(array(':floor_id' => $this->getId()));
		if ($p_get_students === FALSE) throw new DBQueryException('Не удалось получить данные о студентах на этаже', 100, $this->db);
		return $p_get_students->fetchAll();
	}

	public function getRoomsStatistics(){
		$q_get_rooms = 'SELECT rooms.*, rooms.room_id as rid, blocks.number as block_number, floors.*,
			(SELECT COUNT(*) FROM students WHERE room_id = rid) as students_count
			FROM rooms
			INNER JOIN blocks ON blocks.block_id = rooms.block_id
			INNER JOIN floors ON blocks.floor_id = floors.floor_id
			WHERE floors.floor_id = :floor_id
		';
		$p_get_rooms = $this->db->prepare($q_get_rooms);
		$p_get_rooms->execute(array(':floor_id' => $this->getId()));
		if ($p_get_rooms === FALSE) throw new DBQueryException('Не удалось получить статистику по компатам', 100, $this->db);
		$rooms = $p_get_rooms->fetchAll();
		$result = array();
		foreach($rooms as $room){
			$result["{$room['block_number']}-{$room['number']}"] = $room;
		}
		return $result;
	}

	public function addBlock($block_number, $comment){
		$q_ins_block = 'INSERT INTO blocks(floor_id, number, comment) VALUES (:floor_id, :number, :comment)';
		$p_ins_block = $this->db->prepare($q_ins_block);
		$p_ins_block->execute(array(
			':floor_id' => $this->id,
			':number' => ('b-' . $this->getId() . $block_number),
			':comment' => $comment,
		));
		if ($p_ins_block === FALSE) throw new DBQueryException('Не удалось добавить блок', 100, $this->db);
		return new Result(true, 'Блок успешно добавлен',
			array(
				'new_block_id' => $this->db->lastInsertId(),
				'dorm_number' => $this->getDormNumber(),
				'floor_number' => $this->getNumber()
			));
	}
}