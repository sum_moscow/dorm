<?php

	class Dorm{

		private static $DORM_AVAILABLE_NUMBERS = array(2, 6);
		private $number;
		private $db;


		public function __construct($dorm_number, PDO $db){
			if (!in_array($dorm_number, self::$DORM_AVAILABLE_NUMBERS)) throw new InvalidArgumentException('Такого общежития нет');
			$this->number = $dorm_number;
			$this->db = $db;
		}


		public function getFloorsList(){
			$q_get_floors = 'SELECT * FROM floors WHERE dorm_number = :dorm_number ORDER BY dorm_number, number';
			$p_get_floors = $this->db->prepare($q_get_floors);
			$p_get_floors->execute(array(':dorm_number' => $this->number));
			return $p_get_floors->fetchAll();
		}


		public function addFloor($floor_number, $comment){
			if (is_numeric($floor_number) == false) throw new InvalidArgumentException('Неправильный номер этажа');
			$q_ins_floor = 'INSERT INTO floors(number, dorm_number, comment) VALUES(:number, :dorm_number, :comment)';
			$p_ins_floor = $this->db->prepare($q_ins_floor);
			$p_ins_floor->execute(array(':number' => $floor_number, ':dorm_number' => $this->number, ':comment' => $comment));
			if ($p_ins_floor === FALSE) throw new DBQueryException(500, 'Ошибка добавления этажа', $this->db);
			return new Result(true, 'Этаж успешно добавлен', array('data' => $this->getFloorsList()));
		}

	}