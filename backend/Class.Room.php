<?php

	class Room{


		private $room_id;
		private $area;
		private $block;
		private $number;
		private $comment;
		private $repairing;
		private $places_count;
		private $db;
		const DORM_NUMBER_INDEX = 0; // index in array if you explode block-id by '-'
		const FLOOR_NUMBER_INDEX = 1; // index in array if you explode block-id by '-'


		public function __construct($room_id, PDO $db, $create_new = false){
			$this->db = $db;
			$q_get_room = 'SELECT rooms.block_id, rooms.number, rooms.area,
					 rooms.comment, rooms.places_count, rooms.room_id, rooms.repairing
					 FROM rooms
					 INNER JOIN blocks ON blocks.block_id = rooms.block_id
					 WHERE (rooms.room_id = :room_id)';
			$p_get_room = $this->db->prepare($q_get_room);
			$p_get_room->execute(array(':room_id' => $room_id));
			if ($p_get_room === FALSE) throw new DBQueryException('Не удалось получить информацию о комнате', 100, $this->db);
			if ($p_get_room->rowCount() != 1) throw new Exception('Комната не найдена в БД');
			$result = $p_get_room->fetch();
			$this->room_id = $result['room_id'];
			$this->block = new Block($result['block_id'], $this->db);
			$this->setArea($result['area']);
			$this->setComment($result['comment']);
			$this->setNumber($result['number']);
			$this->setPlacesCount($result['places_count']);
			$this->repairing = $result['repairing'];
		}

		/**
		 * @param mixed $repairing
		 */
		public function setRepairing($repairing) {
			$this->repairing = ($repairing == true || $repairing == 1) ? 1 : 0;
			$q_ins_repairing = 'INSERT INTO repairs (room_id, act_type)
				VALUES(:room_id, :act_type)';
			$p_ins_repairing = $this->db->prepare($q_ins_repairing);
			$p_ins_repairing->execute(array(':room_id' => $this->getRoomId(), ':act_type' => $this->repairing));
		}

		/**
		 * @return mixed
		 */
		public function getRepairing() {
			return $this->repairing;
		}


		public function getFullData(){
			return new Result(true, '', array(
				'main' =>
					array(
						'room_id' => $this->getRoomId(),
						'area' => $this->getArea(),
						'comment' =>  $this->getArea(),
						'number' =>  $this->getNumber(),
						'block_number' =>  $this->getBlock()->getNumber(),
						'places_count' =>  $this->getPlacesCount(),
						'repairing' =>  $this->getRepairing()
					),
				'students' =>
					$this->getStudentsList(),
			));
		}

		/**
		 * @return array
		 * @deprecated
		 * @throws DBQueryException
		 */
		public function getStudentsList(){
			$q_get_students = 'SELECT students.*, insts.abbr AS inst_name
				FROM students
				INNER JOIN insts ON insts.inst_id = students.inst_id
				WHERE students.room_id=:room_id';
			$p_get_students = $this->db->prepare($q_get_students);
			$p_get_students->execute(array(':room_id' => $this->getRoomId()));
			if ($p_get_students === FALSE)throw new DBQueryException('Не удалось получить список студентов в комнате', 100, $this->db);
			return $p_get_students->fetchAll();
		}

		public function getStudentsCount(){
			$q_get_count = 'SELECT COUNT(*) AS students_count FROM students WHERE students.room_id = :room_id';
			$p_get_count = $this->db->prepare($q_get_count);
			if ($p_get_count === FALSE) throw new DBQueryException('Ошибка запроса к БД, попробуйте еще раз', 100, $this->db);
			return $p_get_count->fetchColumn(0);
		}

		public function getRoomId(){
			return $this->room_id;
		}

		/**
		 * @param mixed $area
		 */
		public function setArea($area) {
			$this->area = $area;
		}

		/**
		 * @return mixed
		 */
		public function getArea() {
			return $this->area;
		}

		/**
		 * @return mixed
		 */
		public function getBlock() {
			return $this->block;
		}

		/**
		 * @param mixed $comment
		 */
		public function setComment($comment) {
			$this->comment = $comment;
		}

		/**
		 * @return mixed
		 */
		public function getComment() {
			return $this->comment;
		}

		/**
		 * @param mixed $number
		 */
		public function setNumber($number) {
			$this->number = $number;
		}

		/**
		 * @return mixed
		 */
		public function getNumber() {
			return $this->number;
		}

		/**
		 * @param mixed $places_count
		 */
		public function setPlacesCount($places_count) {
			$this->places_count = $places_count;
		}

		/**
		 * @return mixed
		 */
		public function getPlacesCount() {
			return $this->places_count;
		}

		public function update(){
			$q_upd_room = 'UPDATE rooms SET
					area = :area,
					number = :number,
					comment = :comment,
					places_count = :places_count,
					repairing = :repairing
				WHERE room_id = :room_id';
			$p_upd_room = $this->db->prepare($q_upd_room);
			$p_upd_room->execute(array(
				':area' => $this->getArea(),
				':number' => $this->getNumber(),
				':comment' => $this->getComment(),
				':places_count' => $this->getPlacesCount(),
				':repairing' => $this->getRepairing(),
				':room_id' => $this->getRoomId(),
			));
			if ($p_upd_room === FALSE) throw new DBQueryException('Ошибка обновления данных.', 100, $this->db);
			return new Result(true, 'Данные успешно обновлены', array(
				'dorm_number' => $this->getBlock()->getFloor()->getDormNumber(),
				'floor_number' => $this->getBlock()->getFloor()->getNumber()
			));
		}

		public function addStudent(){
			$students_count = $this->getStudentsCount();
			if ( $this->getPlacesCount() != null && (($students_count == $this->getPlacesCount()) || ($students_count > $this->getPlacesCount()))){
				throw new LogicException('Извните, но мы не можем заселить нового студента, количество студентов больше количества мест');
			}else{
				$queue = new Queue($this->db);
				$student_id = $queue->fetchTop($this->block->getStudentsSex())->getId();
				$q_upd_student = 'UPDATE students SET room_id = :room_id WHERE student_id = :student_id';
				$p_upd_student = $this->db->prepare($q_upd_student);
				$p_upd_student->execute(array(
					':student_id' => $student_id,
					':room_id' => $this->getRoomId()
				));
				if ($p_upd_student === FALSE) throw new DBQueryException('Ошибка обновления статуса студента', 100, $this->db);
				return new Result(true, '', array(
					'dorm_number' => $this->block->getFloor()->getDormNumber(),
					'floor_number' => $this->block->getFloor()->getNumber(),
					'student_id' => $student_id

				));
			}
		}

		public function removeStudent($student_id, $reason){
			$q_upd_student = 'UPDATE students SET room_id = null, status = 0 WHERE student_id = :student_id';
			$q_ins_remove = 'INSERT INTO removes(student_id, reason, removed_from)
				VALUES(:student_id, :reason, "room")';
			$this->db->beginTransaction();

			$p_upd_student = $this->db->prepare($q_upd_student);
			$p_upd_student->execute(array(':student_id' => $student_id));
			if ($p_upd_student === FALSE){
				$this->db->rollBack();
				throw new DBQueryException('Ошибка обновления статуса студента', 100, $this->db);
			}
			$p_ins_remove = $this->db->prepare($q_ins_remove);
			$p_ins_remove->execute(array(':student_id' => $student_id, ':reason' => $reason));
			if ($p_ins_remove === FALSE){
				$this->db->rollBack();
				throw new DBQueryException('Ошибка обновления статуса студента', 100, $this->db);
			}

			$this->db->commit();

			return new Result(true, 'Студент успешно удален из комнаты', array(
				'dorm_number' => $this->block->getFloor()->getDormNumber(),
				'floor_number' => $this->block->getFloor()->getNumber(),
				'student_id' => $student_id

			));
		}

	}