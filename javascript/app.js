
"use strict";

String.prototype.capitalize = function () { return this.charAt(0).toUpperCase() + this.slice(1); };


/*TMPL lib by GetLect*/
function tmpl(template_type, items, addTo, direction){

	function replaceTags(html, object){
		$.each(object, function(key, value){
			var key_expr = new RegExp('{'+key+'}', 'gim');
			html = html ? html.replace(key_expr,value) : '';
		});
		var not_handled_keys = new RegExp(/\{.*?\}/gim);
		html = html ? html.replace(not_handled_keys,'') : '';
		return html;
	}

	var result = '',
		html_val = (typeof template_type == 'object')?template_type.tmpl:$('#tmpl-'+template_type).html(), //Добавляю возможность передачу сразу тела шаблона, а не селектора
		comments = new RegExp(/\/\/.*?$|\/\*[\s\S]*?\*\//gim),
		spaces = new RegExp('\\s{2,}','igm');
	if(html_val === undefined || items === undefined){
		console.group('tmpl_error');
		console.log('error in '+template_type);
		console.log('items', items);
		console.log('addTo', addTo);
		console.log('html_val', html_val);
		console.log('inputs', {template_type: template_type, items:items, addTo:addTo, direction:direction});
		console.groupEnd();
	}
	html_val = html_val ? html_val.replace(comments,'') : '';
	html_val = html_val ? html_val.replace(spaces,'').trim() : '';
	if (Array.isArray(items)){
		for (var i in items){
			if (items.hasOwnProperty(i)){
				result += replaceTags(html_val, items[i]);
			}
		}

	} else {
		result = replaceTags(html_val, items);
	}
	result = $(result);
	if (addTo == null || addTo == undefined){
		return result;
	}
	if(direction == 'prepend'){
		addTo.prepend(result);
	}else if ('append'){
		addTo.append(result);
	}else if ('replace'){
		addTo.html(result);
	}
	return result;
}

//TODO: Мебель
//TODO: Поиск
//TODO: Статистика
//TODO: Редактироваие студентов (осталось только в очереди сделать привязку кнопок)
//TODO: Подсказки городов
//TODO: Переселения
//TODO:
function showNotification(text, icon, color_type, callback){ // first argument can be a Result object
	if (typeof text == 'object'){
		if (text.status == true){
			color_type = 'success';
			text = text.text;
			icon = 'check';
		}else{
			color_type = 'error';
			text = text.text;
			icon = 'warning';
		}
	}
	var color_types = {
		'warning': ['#fcf8e3', '#8a6d3b'], //background, text-color
		'error': ['#f2dede', '#a94442'],
		'success': ['#dff0d8', '#3c763d'],
		'info': ['#d9edf7', '#31708f']
	};
	console.log(typeof text, text, color_types, color_type, color_types[color_type]); var notification = new NotificationFx({
			message : tmpl('notification', {'message': text, 'icon': icon, 'color': color_types[color_type][1]}).html(),
			layout : 'attached',
			effect : 'bouncyflip',
			type : 'notice', // notice, warning or error
			onClose : function() {
				if (typeof callback == 'function'){
					callback();
				}
			}
		}),
		notifications_z_index = parseInt($('.ns-box.ns-show:first').css('z-index')) + 2,
		current_ntf = $(notification.ntf); // height of toolbar
	current_ntf.css({
		'top': '50px',
		'z-index': notifications_z_index,
		'background': color_types[color_type][0],
		'color': color_types[color_type][1]
	});
	notification.show();
}


$(document).ready(function(){


	function appendFloors(){
		$.ajax({
			'url' : 'backend/api.php?act=dorms.getFloorsList',
			'dataType' : 'JSON',
			'success' : function(result){
				var dorm_number,
					items = result.data.items,
					current_dorm_btn, floors_in_dorm,
					floors_in_dorm_count, floor, i;
				for (dorm_number in items){
					if (items.hasOwnProperty(dorm_number)){
						current_dorm_btn = $('.select-dorm.dorm-' + dorm_number).find('.dropdown-menu').empty();
						floors_in_dorm = items[dorm_number];
						floors_in_dorm_count = floors_in_dorm.length;
						for (i = 0; i < floors_in_dorm_count; i++){
							floor = floors_in_dorm[i];
							current_dorm_btn.append(tmpl('floor-item', {'dorm_number': dorm_number, 'floor_number': floor.number}).on('click', showFloor));
						}
						current_dorm_btn.append(tmpl('add-floor-item', {'dorm_number': dorm_number}).on('click', showAddFloorModal));
					}
					$('.dorm-2 li.item:first').click();

				}
			}
		});
	}

	function sendAddFloorToDorm(){
		$.ajax({
			url: 'backend/api.php?act=dorm.addFloor',
			data: $('.add-floor-to-dorm-form').serializeArray(),
			dataType: 'JSON',
			type: 'POST',
			success: function(result){
				if (result.status === true){
					//noinspection JSUnresolvedFunction
					window.modal_window.modal('hide');
					showNotification(result);
					appendFloors();
				}
			}
		});
	}

	function sendAddBlock(){
		$.ajax({
			url: 'backend/api.php?act=floor.addBlock',
			data: $('.add-block-to-floor').serializeArray(),
			dataType: 'JSON',
			type: 'POST',
			success: function(res){
				showNotification(res);
				if (res.status == true){
					//noinspection JSUnresolvedFunction
					window.modal_window.modal('hide');
					showFloor(res.data.dorm_number, res.data.floor_number);
				}
			}
		});
	}

	function sendAddRoom(){
		$.ajax({
			url: 'backend/api.php?act=block.addRoom',
			data: $('.add-room-to-block').serializeArray(),
			dataType: 'JSON',
			type: 'POST',
			success: function(res){
				showNotification(res);
				if (res.status == true){
					//noinspection JSUnresolvedFunction
					window.modal_window.modal('hide');
					showFloor(res.data.dorm_number, res.data.floor_number);
				}
			}
		});
	}

	function sendAddStudent(){
		$.ajax({
			url: 'backend/api.php?act=queue.addStudent',
			data: $('.student-modal').serializeArray(),
			dataType: 'JSON',
			type: 'POST',
			success: function(res){
				showNotification(res);
				if (res.status == true){
					//noinspection JSUnresolvedFunction
					window.modal_window.modal('hide');
					showQueue();
				}
			}
		});
	}

	function sendEditStudent(cb){
		$.ajax({
			url: 'backend/api.php?act=student.edit',
			data: $('.student-modal').serializeArray(),
			dataType: 'JSON',
			type: 'POST',
			success: function(res){
				showNotification(res);
				if (res.status == true){
					//noinspection JSUnresolvedFunction
					window.modal_window.modal('hide');
					if (cb instanceof Function){
						cb(res.data.dorm_number, res.data.floor_number);
					}
				}
			}
		});
	}

	function sendRemoveStudent(room_id, student_id, reason){
		$.ajax({
			url: 'backend/api.php?act=rooms.removeStudent',
			dataType: 'JSON',
			type: 'POST',
			data: [{'name': 'room_id', 'value': room_id},{'name': 'student_id', 'value': student_id}, {'name': 'reason', 'value': reason}],
			success: function(res){
				showNotification(res);
				if (res.status == true){
					//noinspection JSUnresolvedFunction
					showFloor(res.data.dorm_number, res.data.floor_number);
				}
			}
		});
	}

	function showAddFloorModal(){
		window.modal_window = tmpl('add-floor-modal', {'dorm_number': $(this).data('dorm-number')});
		//noinspection JSUnresolvedFunction
		window.modal_window.on('show.bs.modal', function(){
			window.modal_window.find('.add-floor-to-dorm').on('click', sendAddFloorToDorm);
		}).modal();
	}

	function showAddBlockModal(){
		var $this = $(this);
		window.modal_window = tmpl('add-block-modal', {'dorm_number': $this.data('dorm-number'), 'floor_number': $this.data('floor-number')});
		//noinspection JSUnresolvedFunction
		window.modal_window.on('show.bs.modal', function(){
			window.modal_window.find('.add-block-to-floor-btn').on('click', sendAddBlock);
		}).modal();
	}

	function showAddRoomModal(){
		var $this = $(this);
		window.modal_window = tmpl('add-room-modal', {'block_number': $this.data('block-number'), 'block_id': $this.data('block-id')});
		//noinspection JSUnresolvedFunction
		window.modal_window.on('show.bs.modal', function(){
			window.modal_window.find('.add-room-to-block-btn').on('click', sendAddRoom);
		}).modal();
	}

	function showAddStudentModal(){
		var $this = $(this);
		window.modal_window = tmpl('add-student-modal', {'modal_name': 'Добавление нового студента в очередь'});
		//noinspection JSUnresolvedFunction
		window.modal_window.on('show.bs.modal', function(){
			window.modal_window.find('.student-modal-btn').on('click', sendAddStudent);
		}).modal();
	}


	function showEditStudentModal(student_id, cb){
		$.ajax({
			url: 'backend/api.php?act=student.getFullData',
			method: 'POST',
			data: {'student_id': student_id},
			dataType: 'JSON',
			success: function(res){
				if (res.status == 1){
					res.data.modal_name = 'Редактирование данных студента ' + res.data.name;
					var $modal = tmpl('add-student-modal', res.data);

					$modal.find('.sex[value="' + res.data.sex + '"]').attr('selected','selected');
					$modal.find('.category[value="' + res.data.category + '"]').attr('selected','selected');
					$modal.find('.inst_id[value="' + res.data.inst_id + '"]').attr('selected','selected');

					window.modal_window = $modal;
					//noinspection JSUnresolvedFunction
					window.modal_window.on('show.bs.modal', function(){
						window.modal_window.find('.student-modal-btn').html('<span class="glyphicon glyphicon-ok"></span> Сохранить').on('click', function(){
							sendEditStudent(cb);
						});
					}).modal();
				}
			}
		});
	}


	function setRoomParam(room_id, param_name, value){
		$.ajax({
			url: 'backend/api.php?act=rooms.set' + param_name.capitalize(),
			method: 'POST',
			data: [{'name': 'value', 'value' : value}, {'name': 'room_id', 'value': room_id}],
			dataType: 'JSON',
			success: function(res){
				showNotification(res);
				if (res.status == true){
					showFloor(res.data.dorm_number, res.data.floor_number);
				}
			}
		});
	}

	function setStudentParam(student_id, param_name, value){
		$.ajax({
			url: 'backend/api.php?act=student.set' + param_name.capitalize(),
			method: 'POST',
			data: [{'name': 'value', 'value' : value}, {'name': 'student_id', 'value': student_id}],
			dataType: 'JSON',
			success: showNotification
		});
	}

	function addNewStudent(room_id){
		return $.ajax({
			url: 'backend/api.php?act=rooms.addStudent',
			method: 'POST',
			data: {'room_id': room_id},
			dataType: 'JSON',
			success: function(res){
				showNotification(res);
				if (res.status == true){
					showFloor(res.data.dorm_number, res.data.floor_number);
				}
			}
		});
	}

	function showRoomInfo(){
		var $this = $(this),
			data = $this.data('room-info'),
			full_room_id = $this.data('full-room-id'),
			last_item_index = $('.line').index($('.' + full_room_id + ':last')),
			$line;

		window.opened_rooms.push(full_room_id); // store opened rooms array

		data.repairing_active = (data.repairing == 1) ? 'active btn-warning' : 'btn-default';
		data.repairing_text   = (data.repairing == 1) ? 'В ремонте'          : 'Активен';

		var	$room_info_line = tmpl('room-info-line', data),
			room_id = data.room_id,
			$area_input = $room_info_line.find('.room-info-area'),
			$max_students_input = $room_info_line.find('.room-info-places-count'),
			$repairing_btn = $room_info_line.find('.repairing-btn');
		$('#ri-' + data.full_room_id).remove();
		do {
			$line = $('.line:eq(' + last_item_index + ')');
			last_item_index++;
		}while($line.next().is('.student-line') == false && $line.next().length == 1);

		$room_info_line.find(' .close-room-info').on('click', function(){
			$('#ri-' + data.full_room_id).remove();
			var index = window.opened_rooms.indexOf(data.full_room_id);
			if (index > -1) {
				window.opened_rooms.splice(index, 1);
			}
		});
		$line.after($room_info_line);

		$area_input
			.on('focus', function(){
				$(this).data('current-value', this.value);
			})
			.on('blur', function(){
				var $input = $(this);
				if ($input.data('current-value') != this.value){
					setRoomParam(room_id, 'area', this.value);
					$input.data('current-value', this.value);
				}
			});

		$max_students_input
			.on('focus', function(){
				$(this).data('current-value', this.value);
			})
			.on('blur', function(){
				var $input = $(this);
				if ($('.student-line.' + full_room_id).length > this.value && $input.data('current-value') > this.value){
					showNotification('Извините, но после этого количество мест станет меньше количества студентов, сначала удалите студентов', 'warning', 'error');
					this.value = $input.data('current-value');
					return true;
				}else if ($input.data('current-value') != this.value){
					setRoomParam(room_id, 'placesCount', this.value);
					$input.data('current-value', this.value);
					return true;
				}
				return true;
			});
	}

	function incRowSpan($el, number){
		number = (isNaN(parseInt(number))) ? 1 : parseInt(number);
		$el.attr('rowspan', parseInt($el.attr('rowspan')) + number);
	}

	function renderStudentsTable(students){
		var student, i,
			students_count = students.length,
			add_class = [],
			$block_td,
			$room_tr,
			$student_tr,
			$table = $('#floor-table'),
			row_spans_sum = 0,
			can_cancel = false,
			now = new Date(),
			add_time,
			student_id,
			no_students_rowspan = 7;

		for (i = 0; i < students_count; i++){
			student = students[i];

			student.repairing_label = (student.repairing == true) ? '<br><span class="label label-danger">В ремонте</span> ' : '';
			student.places_count_label = (isNaN(parseInt(student.places_count)) == false) ? ('<br><strong>Мест: </strong><span>'+student.places_count+'</span> ') : '';
			student.for_family_label = (student.for_family == 1) ? '<br><span class="label label-info">Семейная</span> ' : '';
			student.area_label = (isNaN(parseInt(student.area)) == false) ? ('<br><strong>Площадь: </strong><span>'+student.area+' м<sup>2</sup></span> ') : '';

			$block_td = $('#' + student.block_number);
			student.full_room_id = (student.full_room_id == null) ? student.block_number + '-00' : student.full_room_id;
			$room_tr = $('#' + student.full_room_id);
			$student_tr = tmpl('student-line', student);
			if ($block_td.length == 1){
				$student_tr.find('td.block').remove();
				incRowSpan($block_td);
			}else{
				$student_tr.find('td.block').data('block-info', student);
			}


			if (student.student_id == null){
				if (student.full_room_id == student.block_number + '-00'){
					$student_tr.find('.room').remove();
					no_students_rowspan++;
				}
				$student_tr.addClass('not-student').find('td.gt3').remove();
				$student_tr.find('td.eq2').attr('colspan', no_students_rowspan).text('Студентов нет');
			}

			if ($room_tr.length == 1){
				$student_tr.find('td.room').remove();
				incRowSpan($room_tr);
			}else{
				$student_tr.find('td.room').data('room-info', student);
			}


			add_time = new Date(student.add_time);
			can_cancel = (((now - add_time) / 60000) < 6); // minutes
			if (can_cancel == false){
				$student_tr.find('td.cancel').remove();
			}

			student_id = student.student_id;
			$student_tr.appendTo($table);
			$student_tr.find(' .edit').on('click', function(){
				var $this = $(this);
				showEditStudentModal($this.parents('tr').data('student-id'), showFloor);
			});
			$student_tr.find(' .comment').on('blur', function(){
				var $this = $(this);
				setStudentParam($this.parents('tr').data('student-id'), 'comment', $this.text());
			});
			$student_tr.find('li.remove-reason').on('click', function(){
				var $this = $(this);
				sendRemoveStudent($this.parents('td').siblings('.room').data('room-id'), $this.parents('tr').data('student-id'), $this.text());
			});
		}
		$('.room').on('click', showRoomInfo).each(function(){
			var $this = $(this),
				$block = ($this.siblings('.block').length == 1) ? $this.siblings('.block') : $('#' + $this.data('block-number')),
				$add_student_line = tmpl('add-student-line', {'room_id': $this.data('room-id')}),
				places_count = $this.data('places-count');
			if (isNaN(parseInt(places_count)) == false && places_count > $('.student-line.' + $this.data('full-room-id') + ':not(.not-student)').length){
				$add_student_line.on('click', function(){
					console.log($this.data('room-id'));
					addNewStudent($this.data('room-id'));
				});
			}else if(isNaN(parseInt(places_count))){
				$add_student_line
					.toggleClass('hover-td-info hover-td-warning')
					.attr('title', 'Не указано количество мест. Возможно переполнение')
					.on('click', function(){
						addNewStudent($this.data('room-id'))
					});
			}else{
				$add_student_line
					.attr('title', 'Заселение невозможно, произошло переполнение')
					.toggleClass('hover-td-info hover-td-danger')
					.on('click', function(){
						showNotification({
							status: false,
							icon: 'warning',
							text: 'Заселение невозможно, комната заполнена'
						})
					});
			}
			row_spans_sum += parseInt(this.getAttribute('rowspan'));
			$('.student-line:eq(' + (row_spans_sum - 1) + ')').after($add_student_line);
			incRowSpan($this);
			incRowSpan($block);
		});
		row_spans_sum = 0;
		$('.block').each(function(){
			var $this = $(this),
				$add_room_line = tmpl('add-room-line', {'block_number': $this.data('block-number'), 'block_id': $this.data('block-id')});
			$add_room_line.on('click', showAddRoomModal);
			row_spans_sum += parseInt(this.getAttribute('rowspan'));
			$('.line:eq(' + (row_spans_sum - 1) + ')').after($add_room_line);
			row_spans_sum++;
			incRowSpan($this);
		});

		var $floor_info = $('.floor-info');
		tmpl('add-block-line', {
				'dorm_number': $floor_info.data('dorm-number'),
				'floor_number': $floor_info.data('floor-number')
			})
			.appendTo($table)
			.on('click', showAddBlockModal);

		/*reopen rooms*/
		var opened = window.opened_rooms,
			opened_length = window.opened_rooms.length;
		window.opened_rooms = [];
		for(i = 0; i < opened_length; i++){
			var room_id = opened[i];
			$('#' + room_id).click();
		}
	}

	function showFloor(dorm_number, floor_number){
		var $this    = $(this);
		dorm_number  = (isNaN(parseInt(dorm_number)))  ? $this.data('dorm-number') : dorm_number;
		floor_number = (isNaN(parseInt(floor_number))) ? $this.data('floor-number'): floor_number;
		$.ajax({
			url: 'backend/api.php?act=floor.getFullData',
			method: 'POST',
			data: [{'name': 'dorm_number', 'value' : dorm_number},
				{'name': 'floor_number', 'value': floor_number}],
			dataType: 'JSON',
			success: function(res){
				var res_data = res.data;
				$('.jumbotron, #plan-wrapper, #queue-wrapper').remove();
				tmpl('floor-dorm', {
					'dorm_number': res_data.dorm_number,
					'floor_number': res_data.floor_number,
					'all_free_spaces': res_data.all_free_spaces
				}, $('.mn-cnt'), 'replace');
				renderStudentsTable(res_data.students);
			}
		});
	}

	function renderQueueTable(students){
		var $student_line, i, student,
			students_count = students.length,
			$queue_table = $('#queue-table');

		for (i = 0; i < students_count; i++){
			student = students[i];
			student.number = (i+1);
			$student_line = tmpl('queue-student-line', student, $queue_table, 'append');
		}
		tmpl('queue-add-student-line', {}, $queue_table).on('click', showAddStudentModal)
	}

	function showQueue(){
		$.ajax({
			url: 'backend/api.php?act=queue.getFullData',
			method: 'POST',
			dataType: 'JSON',
			success: function(res){
				var res_data = res.data;
				$('.jumbotron, #plan-wrapper, #queue-wrapper').remove();
				tmpl('queue-list', res_data, $('.mn-cnt'), 'replace');
				renderQueueTable(res_data.students);
			}
		});
	}

	/* Один раз отрендерим и будем хранить в памяти, чтобы при каждой загрузке не делать это */
	var cap = $('#cap'),
		cap_wr = $('#cap-wrapper'),
		warning_html = tmpl('ajax-warning', {'message': 'Произошла ошибка при загрузке. Попробуйте еще раз, пожалуйста...'}),
		loading_html = tmpl('ajax-loading', {});

	window.paceOptions = {
		elements: false,
		ajax: true, // enabled
		document: false, // disabled
		eventLag: false, // disabled
		restartOnRequestAfter: true
	};
	window.opened_rooms = [];
	$( document ).ajaxError(function(){
		cap_wr.html(warning_html)
			.off('click')
			.on('click', function(){
				cap.removeClass('show');
			}).find('.close').off('click').on('click', function(){
				cap.removeClass('show');
			});
	});

	$( document ).ajaxStart(function(){
		cap_wr.html(loading_html);
		cap.addClass('show');
		Pace.restart();
	});

	//noinspection FunctionWithInconsistentReturnsJS
	$( document ).ajaxSuccess(function(event, xhr){
		if (xhr.responseJSON.status == false){
			cap_wr.html(tmpl('ajax-warning', {'message': xhr.responseJSON.text}))
				.off('click')
				.on('click', function(){
					cap.removeClass('show');
				}).find('.close').off('click').on('click', function(){
					cap.removeClass('show');
				});
			event.preventDefault();
			return false;
		}else{
			cap.removeClass('show');
		}
	});

	appendFloors(); // App init
	$('.show-queue').on('click', showQueue);
});